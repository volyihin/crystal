package ru.crystals.service;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import ru.crystals.entity.Price;
import ru.crystals.exception.MergeException;
import ru.crystals.util.PriceMerger;

public class PriceService {

	private static final Logger logger = Logger.getLogger(PriceService.class);

	/**
	 * Объединяет цены по следующим правилам:
	 * <ul>
	 * <li>если товар еще не имеет цен, или имеющиеся цены не пересекаются в
	 * периодах действия с новыми, то новые цены просто добавляются к товару;
	 * <li>если имеющаяся цена пересекается в периоде действия с новой ценой,
	 * то:
	 * <ul>
	 * <li>если значения цен одинаковы, период действия имеющейся цены
	 * увеличивается согласно периоду новой цены;
	 * <li>если значения цен отличаются, добавляется новая цена, а период
	 * действия старой цены уменьшается согласно периоду новой цены.
	 * </ul>
	 * </ul>
	 * 
	 * 
	 * @param newPrices
	 * @param oldPricesList
	 *            - результат объединения
	 * @return
	 * @throws MergeException
	 */
	public List<Price> mergePrices(List<Price> newPrices, List<Price> oldPricesList) throws MergeException {

		Iterator<Price> newPricesIterator = newPrices.iterator();

		while (newPricesIterator.hasNext()) {
			Price newPrice = newPricesIterator.next();
			newPricesIterator.remove();
			logger.info("Новая цена: " + newPrice.toString());
			int oldPriceCount = 0;
			for (Price oldPrice : oldPricesList) {

				if (PriceMerger.needMerge(newPrice, oldPrice)) {

					if (newPrice.getValue() == oldPrice.getValue()) {
						PriceMerger.mergePriceSameValue(newPrice, oldPrice, oldPricesList);
						break;

					} else {
						if (newPrice.getInterval().overlaps(oldPrice.getInterval())) {
							PriceMerger.mergePriceDiffValueOverlapInterval(newPrice, oldPrice, oldPricesList);
							break;
						}
					}

				}

				oldPriceCount++;

			}
			// если дошли до последнего - значит просто добавляем в старым ценам
			if (oldPricesList.size() == oldPriceCount)
				oldPricesList.add(newPrice);

		}

		return oldPricesList;
	}

}
