package ru.crystals.util;

import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.Interval;

import ru.crystals.entity.Price;
import ru.crystals.exception.MergeException;

public class PriceMerger {

	private static final Logger logger = Logger.getLogger(PriceMerger.class);

	/**
	 * Сливает цены с одинаковым значением.
	 * 
	 * Если интревалы пересекаются - то возвращется однеа цена с объединенным
	 * интервалом, если нет - то к старым ценам добавляется новая
	 * 
	 * @param newPrice
	 * @param oldPrice
	 * @param oldPricesList
	 * @throws MergeException
	 */
	public static void mergePriceSameValue(Price newPrice, Price oldPrice, List<Price> oldPricesList)
			throws MergeException {

		logger.info("сливаем цены с одинаковым значением: " + newPrice.toString() + oldPrice.toString());

		try {

			if (newPrice.getInterval().overlaps(oldPrice.getInterval())) {
				Interval union = Intervals.union(newPrice.getInterval(), oldPrice.getInterval());
				oldPrice.setInterval(union);
			} else {
				oldPricesList.add(newPrice);
				logger.info("результат: " + newPrice.toString());
			}
			logger.info("результат: " + oldPrice.toString());
		} catch (Exception e) {
			logger.warn("Ошибка слияния цен с одинаковым значением.", e);
			throw new MergeException("Ошибка слияния цен с одинаковым значением.", e);
		}

	}

	/**
	 * Сливает цены с разными значениями, но пересекающимися интервалами
	 * 
	 * @param newPrice
	 * @param oldPrice
	 * @param oldPricesList
	 * @throws MergeException
	 */
	public static void mergePriceDiffValueOverlapInterval(Price newPrice, Price oldPrice, List<Price> oldPricesList)
			throws MergeException {
		logger.info("сливаем цены c разными значениями, но пересекающимися интервалами: " + newPrice.toString()
				+ oldPrice.toString());
		try {
			Interval overlap = newPrice.getInterval().overlap((oldPrice.getInterval()));
			List<Interval> intervals = Intervals.substraction(oldPrice.getInterval(), overlap);
			if (intervals.size() == 2) {
				Price oldPrice2;
				try {
					oldPrice2 = oldPrice.clone();
					oldPrice2.setInterval(intervals.get(1));
					oldPricesList.add(oldPrice2);
					logger.info("результат: " + oldPrice2.toString());
				} catch (CloneNotSupportedException e) {
					logger.warn("Не удалось создать копию объекта ", e);
					throw new MergeException("Не удалось создать копию объекта", e);
				}
			}
			oldPrice.setInterval(intervals.get(0));
			logger.info("результат: " + oldPrice.toString());
			logger.info("результат: " + newPrice.toString());
			oldPricesList.add(newPrice);

		} catch (Exception e) {
			logger.warn("Ошибка слияния цен с разными значениями, но пересекающимися интервалами.", e);
			throw new MergeException("Ошибка слияния цен с разными значениями, но пересекающимися интервалами.", e);
		}
	}

	/**
	 * Условия слияния:
	 * <ul>
	 * <li>одинаковый код продукта
	 * <li>одинаковый номер продукта
	 * <li>одинаковый номер отдела
	 * </ul>
	 * 
	 * 
	 * @param newPrice
	 * @param oldPrice
	 * @return
	 */
	public static boolean needMerge(Price newPrice, Price oldPrice) {
		if ((newPrice.getProduct_code() == oldPrice.getProduct_code())
				&& (newPrice.getNumber() == oldPrice.getNumber()) && (newPrice.getDepart() == oldPrice.getDepart()))
			return true;
		return false;
	}

}
