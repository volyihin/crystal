package ru.crystals.util;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import static com.google.common.collect.Lists.*;

/**
 * Класс для работы с интервалами. Объединение, разность интервалов.
 * 
 * @author d.volyhin
 * 
 */
public class Intervals {

	/**
	 * Объединение интервалов
	 * 
	 * @param firstInterval
	 * @param secondInterval
	 * @return
	 */
	public static Interval union(Interval firstInterval, Interval secondInterval) {

		DateTime start = firstInterval.getStart().isBefore(
				secondInterval.getStart()) ? firstInterval.getStart()
				: secondInterval.getStart();
		DateTime end = firstInterval.getEnd().isAfter(secondInterval.getEnd()) ? firstInterval
				.getEnd() : secondInterval.getEnd();
		Interval unionInterval = new Interval(start, end);

		return unionInterval;
	}

	/**
	 * Разность интевалов.
	 * 
	 * @param firstInterval
	 * @param secondInterval
	 * @return
	 */
	public static List<Interval> substraction(Interval firstInterval,
			Interval secondInterval) {
		List<Interval> intervals = newArrayList();

		// если совпадает начало интервала
		if (firstInterval.getStart().equals(secondInterval.getStart())) {
			if (firstInterval.getEnd().isAfter(secondInterval.getEnd())) {
				return newArrayList(new Interval(secondInterval.getEnd(),
						firstInterval.getEnd()));
			} else {
				return newArrayList(new Interval(firstInterval.getEnd(),
						secondInterval.getEnd()));
			}
		}
		// если совпадает конец интервала
		if (firstInterval.getEnd().equals(secondInterval.getEnd())) {
			if (firstInterval.getStart().isAfter(secondInterval.getStart())) {
				return newArrayList(new Interval(secondInterval.getStart(),
						firstInterval.getStart()));
			} else {
				return newArrayList(new Interval(firstInterval.getStart(),
						secondInterval.getStart()));
			}
		}
		// если второй интервал внутри первого
		if ((firstInterval.getStart().isBefore(secondInterval.getStart()))
				&& ((firstInterval.getEnd().isAfter(secondInterval.getStart())))) {
			Interval interval1 = new Interval(firstInterval.getStart(),
					secondInterval.getStart());
			Interval interval2 = new Interval(secondInterval.getEnd(),
					firstInterval.getEnd());
			intervals.add(interval1);
			intervals.add(interval2);
			return intervals;
		}
		// если первый внутри второго(нужно проверить вызвать
		// substraction(secondInterval,firstInterval)
		if ((firstInterval.getStart().isAfter(secondInterval.getStart()))
				&& ((firstInterval.getEnd().isBefore(secondInterval.getStart())))) {
			Interval interval1 = new Interval(secondInterval.getStart(),
					firstInterval.getStart());
			Interval interval2 = new Interval(firstInterval.getEnd(),
					secondInterval.getEnd());
			intervals.add(interval1);
			intervals.add(interval2);
			return intervals;

		}
		// значит интервалы равны
		return newArrayList(firstInterval);

	}
}
