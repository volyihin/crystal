package ru.crystals.entity;

import java.util.Comparator;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;

public class Price implements Cloneable {

	public static final String DATE_FORMAT = "dd.M.YYYY HH:mm:SS";

	@Override
	public String toString() {
		return "P[" + product_code + " " + number + " " + depart + " "
				+ interval.getStart().toString(DateTimeFormat.forPattern(DATE_FORMAT)) + " "
				+ interval.getEnd().toString(DateTimeFormat.forPattern(DATE_FORMAT)) + " " + value
				+ " " + "]\n";
	}

	public Price() {
	}

	public Price(long product_code, int number, int depart, DateTime begin, DateTime end, long value) {
		super();
		this.product_code = product_code;
		this.number = number;
		this.depart = depart;
		this.interval = new Interval(begin, end);
		this.value = value;
	}

	/**
	 * идентификатор в БД
	 */
	private long id;
	/**
	 * код товара
	 */
	private long product_code;
	/**
	 * номер цены
	 */
	private int number;
	/**
	 * номер отдела
	 */
	private int depart;
	/**
	 * интервал действия цены
	 */
	private Interval interval;
	/**
	 * значение цены в копейках
	 */
	private long value;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getProduct_code() {
		return product_code;
	}

	public void setProduct_code(long product_code) {
		this.product_code = product_code;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getDepart() {
		return depart;
	}

	public void setDepart(int depart) {
		this.depart = depart;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public static Comparator<Price> getBeginDateComparator() {
		return new Comparator<Price>() {

			public int compare(Price o1, Price o2) {
				if (o1.getInterval().getStart().getMillis() < o2.getInterval().getStart()
						.getMillis())
					return -1;
				if (o1.getInterval().getStart().getMillis() > o2.getInterval().getStart()
						.getMillis())
					return 1;
				return 0;
			}
		};
	}

	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + depart;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((interval == null) ? 0 : interval.hashCode());
		result = prime * result + number;
		result = prime * result + (int) (product_code ^ (product_code >>> 32));
		result = prime * result + (int) (value ^ (value >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Price other = (Price) obj;
		if (depart != other.depart)
			return false;
		if (id != other.id)
			return false;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		if (number != other.number)
			return false;
		if (product_code != other.product_code)
			return false;
		if (value != other.value)
			return false;
		return true;
	}

	@Override
	public Price clone() throws CloneNotSupportedException {
		Price clone = new Price(this.product_code, this.number, this.depart,
				this.interval.getStart(), this.interval.getEnd(), this.value);
		return clone;
	}

}
