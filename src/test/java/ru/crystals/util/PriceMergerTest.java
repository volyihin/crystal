package ru.crystals.util;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import ru.crystals.entity.Price;
import ru.crystals.exception.MergeException;
import ru.crystals.service.PriceServiceTest;

import com.google.common.collect.Lists;

public class PriceMergerTest {

	@Test
	public void testMergePriceSameValue_overlapsInterval() throws MergeException {

		Price newPrice = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 31.01.2013 23:59:59 11000");
		Price oldPrice = PriceServiceTest.fromString("122856 1 1 20.01.2013 00:00:00 20.02.2013 23:59:59 11000");
		List<Price> oldPricesList = Lists.newArrayList();

		Price expectedPrice = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 20.02.2013 23:59:59 11000");

		PriceMerger.mergePriceSameValue(newPrice, oldPrice, oldPricesList);

		assertEquals(expectedPrice, oldPrice);

	}

	@Test
	public void testMergePriceSameValue_NotOverlapsInterval() throws MergeException {

		Price newPrice = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 31.01.2013 23:59:59 11000");
		Price oldPrice = PriceServiceTest.fromString("122856 1 1 20.02.2013 00:00:00 20.03.2013 23:59:59 11000");
		List<Price> oldPricesList = Lists.newArrayList(oldPrice);

		PriceMerger.mergePriceSameValue(newPrice, oldPrice, oldPricesList);
		assertTrue(oldPricesList.contains(newPrice));
		assertTrue(oldPricesList.contains(oldPrice));

	}

	/**
	 * Результат - 2 интервала, новый перекрывает старый
	 * 
	 * @throws MergeException
	 */
	@Test
	public void testMergePriceDiffValueOverlapInterval_2Intervals() throws MergeException {

		Price newPrice = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 31.01.2013 23:59:59 11000");
		Price oldPrice = PriceServiceTest.fromString("122856 1 1 20.01.2013 00:00:00 20.03.2013 23:59:59 12000");
		List<Price> oldPricesList = Lists.newArrayList(oldPrice);

		Price expectedOldPrice = PriceServiceTest
				.fromString("122856 1 1 31.01.2013 23:59:59 20.03.2013 23:59:59 12000");

		PriceMerger.mergePriceDiffValueOverlapInterval(newPrice, oldPrice, oldPricesList);

		assertTrue(oldPricesList.contains(newPrice));
		assertTrue(oldPricesList.contains(expectedOldPrice));

	}

	/**
	 * Результат - 3 интервала, новый разбивает старый на 2
	 * 
	 * @throws MergeException
	 */
	@Test
	public void testMergePriceDiffValueOverlapInterval_3Intervals() throws MergeException {

		Price newPrice = PriceServiceTest.fromString("122856 1 1 10.01.2013 00:00:00 20.01.2013 23:59:59 12000");
		Price oldPrice = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 31.01.2013 23:59:59 11000");
		List<Price> oldPricesList = Lists.newArrayList(oldPrice);

		Price expectedOldPriceLeft = PriceServiceTest
				.fromString("122856 1 1 01.01.2013 00:00:00 10.01.2013 00:00:00 11000");
		Price expectedOldPriceRight = PriceServiceTest
				.fromString("122856 1 1 20.01.2013 23:59:59 31.01.2013 23:59:59 11000");

		PriceMerger.mergePriceDiffValueOverlapInterval(newPrice, oldPrice, oldPricesList);

		assertTrue(oldPricesList.contains(newPrice));
		assertTrue(oldPricesList.contains(expectedOldPriceLeft));
		assertTrue(oldPricesList.contains(expectedOldPriceRight));

	}
}
