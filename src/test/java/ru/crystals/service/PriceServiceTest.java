package ru.crystals.service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import ru.crystals.entity.Price;
import ru.crystals.exception.MergeException;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;

public class PriceServiceTest {

	private static final String NEW_PRICE = "Новые цены";
	private static final String RESULT = "Результат:";

	private static final Logger logger = Logger.getLogger(PriceServiceTest.class);

	private void testMergePriciesForTestCase(String path) throws URISyntaxException, FileNotFoundException,
			MergeException {
		URL url = Resources.getResource(path);
		List<Price> resultPrices = Lists.newArrayList();
		List<Price> oldPricies = Lists.newArrayList();
		List<Price> newPricies = Lists.newArrayList();
		List<Price> expectedPricies = Lists.newArrayList();
		try (Scanner scanner = new Scanner(new File(url.toURI()))) {
			boolean isNew = false;
			boolean isResult = false;
			while (scanner.hasNext()) {
				String priceLine = scanner.nextLine();
				if (priceLine.contains(RESULT)) {
					isResult = true;
					isNew = false;
					continue;
				}
				if (priceLine.contains(NEW_PRICE)) {
					isNew = true;
					continue;
				}

				Price p = fromString(priceLine);

				if (isResult) {
					expectedPricies.add(p);
				} else if (isNew) {
					newPricies.add(p);
				} else
					oldPricies.add(p);
			}
			PriceService priceService = new PriceService();
			resultPrices = priceService.mergePrices(newPricies, oldPricies);
		}

		logger.info("Ожидали");
		logger.info(expectedPricies.toString());
		logger.info("Получили");
		logger.info(resultPrices.toString());

		assertTrue(resultPrices.containsAll(expectedPricies));
		assertTrue(resultPrices.size() == expectedPricies.size());
	}

	@Test
	public void testMergePrices_testcase1() throws URISyntaxException, FileNotFoundException, MergeException {
		testMergePriciesForTestCase("testcase1.txt");
	}

	@Test
	public void testMergePrices_testcase2() throws URISyntaxException, FileNotFoundException, MergeException {
		testMergePriciesForTestCase("testcase2.txt");
	}

	@Test
	public void testMergePrices_testcase3() throws URISyntaxException, FileNotFoundException, MergeException {
		testMergePriciesForTestCase("testcase3.txt");

	}

	@Test
	public void testMergePrices_testcase4() throws URISyntaxException, FileNotFoundException, MergeException {
		testMergePriciesForTestCase("testcase4.txt");

	}

	/**
	 * Одинаковые цены
	 * @throws MergeException 
	 */
	@Test
	public void testMergePrice_SamePrice() throws MergeException {
		Price p1 = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 31.01.2013 23:59:59 11000");
		Price p2 = PriceServiceTest.fromString("122856 1 1 20.01.2013 00:00:00 20.02.2013 23:59:59 11000");
		List<Price> newPricies = Lists.newArrayList();
		List<Price> oldPrices = Lists.newArrayList();
		newPricies.add(p1);
		oldPrices.add(p2);

		Price resultPrice = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 20.02.2013 23:59:59 11000");

		PriceService priceService = new PriceService();
		List<Price> resultPricies = priceService.mergePrices(newPricies, oldPrices);

		assertEquals(resultPricies.get(0), resultPrice);
	}

	/**
	 * Разные цены
	 * @throws MergeException 
	 */
	@Test
	public void testMergePrice_DiffPrice() throws MergeException {
		Price p1 = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 31.01.2013 23:59:59 11000");
		Price p2 = PriceServiceTest.fromString("122856 1 1 20.01.2013 00:00:00 20.02.2013 23:59:59 12000");
		List<Price> newPricies = Lists.newArrayList();
		List<Price> oldPrices = Lists.newArrayList();
		newPricies.add(p1);
		oldPrices.add(p2);

		Price p1Expected = PriceServiceTest.fromString("122856 1 1 01.1.2013 00:00:00 31.1.2013 23:59:59 11000");
		Price p2Expected = PriceServiceTest.fromString("122856 1 1 31.1.2013 23:59:59 20.2.2013 23:59:59 12000");

		PriceService priceService = new PriceService();
		List<Price> resultPricies = priceService.mergePrices(newPricies, oldPrices);

		assertTrue(resultPricies.contains(p1Expected));
		assertTrue(resultPricies.contains(p2Expected));
	}

	/**
	 * Разные цены
	 * @throws MergeException 
	 */
	@Test
	public void testMergePrice_testcase11() throws MergeException {

		Price newPrice1 = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 10.01.2013 23:59:59 11000");
		Price newPrice2 = PriceServiceTest.fromString("122856 1 1 15.01.2013 00:00:00 25.01.2013 23:59:59 12000");
		List<Price> newPricies = Lists.newArrayList();
		newPricies.add(newPrice1);
		newPricies.add(newPrice2);

		List<Price> oldPrices = Lists.newArrayList();
		Price oldPrice1 = PriceServiceTest.fromString("122856 1 1 11.01.2013 00:00:00 12.01.2013 23:59:59 11000");
		Price oldPrice2 = PriceServiceTest.fromString("122856 1 1 13.01.2013 00:00:00 14.01.2013 23:59:59 11000");
		oldPrices.add(oldPrice1);
		oldPrices.add(oldPrice2);

		PriceService priceService = new PriceService();
		List<Price> resultPricies = priceService.mergePrices(newPricies, oldPrices);
		assertTrue(resultPricies.contains(newPrice1));
		assertTrue(resultPricies.contains(newPrice2));
		assertTrue(resultPricies.contains(oldPrice1));
		assertTrue(resultPricies.contains(oldPrice2));
	}

	@Test
	public void testMergePrices_testcase12() throws MergeException {

		Price newPrice1 = PriceServiceTest.fromString("122856 1 1 01.01.2013 00:00:00 10.01.2013 23:59:59 11000");
		Price newPrice2 = PriceServiceTest.fromString("122856 1 1 15.01.2013 00:00:00 25.01.2013 23:59:59 12000");
		List<Price> newPricies = Lists.newArrayList();
		newPricies.add(newPrice1);
		newPricies.add(newPrice2);

		List<Price> oldPrices = Lists.newArrayList();
		Price oldPrice1 = PriceServiceTest.fromString("122856 1 1 11.01.2013 00:00:00 16.01.2013 23:59:59 12000");
		Price oldPrice2 = PriceServiceTest.fromString("122856 1 1 13.01.2013 00:00:00 14.01.2013 23:59:59 11000");
		oldPrices.add(oldPrice1);
		oldPrices.add(oldPrice2);

		Price expectedPrice = PriceServiceTest.fromString("122856 1 1 11.01.2013 00:00:00 25.01.2013 23:59:59 12000");

		PriceService priceService = new PriceService();
		List<Price> resultPricies = priceService.mergePrices(newPricies, oldPrices);

		assertTrue(resultPricies.contains(newPrice1));
		assertTrue(resultPricies.contains(oldPrice2));
		assertTrue(resultPricies.contains(expectedPrice));

	}

	public static Price fromString(String priceLine) {

		String[] arr = Iterables.toArray(Splitter.on(" ").omitEmptyStrings().trimResults().split(priceLine),
				String.class);

		long product_code = Long.valueOf(arr[0]).longValue();
		int number = Integer.valueOf(arr[1]).intValue();
		int depart = Integer.valueOf(arr[2]).intValue();
		DateTimeFormatter formatter = DateTimeFormat.forPattern(Price.DATE_FORMAT);
		DateTime begin = DateTime.parse(arr[3] + " " + arr[4], formatter);
		DateTime end = DateTime.parse(arr[5] + " " + arr[6], formatter);
		long value = Long.valueOf(arr[7]).longValue();

		Price price = new Price(product_code, number, depart, begin, end, value);

		return price;

	}

}
